logistics = {}
logistics.modpath = minetest.get_modpath("logistics")

dofile(logistics.modpath.."/lib.lua")

-- logistics drone velocity (movement speed) settings
logistics.drone_vscale = minetest.settings:get("logistics.drone_speed")

if logistics.drone_vscale == nil then
    logistics.drone_vscale = 2
end

logistics.drone_vmin = logistics.drone_vscale
logistics.drone_vmax = logistics.drone_vscale * 3


-- logistics drone cargo size
logistics.drone_cargo_size = minetest.settings:get("logistics.drone_cargo_size")

if logistics.drone_cargo_size == nil then
    logistics.drone_cargo_size = 1
end

dofile(logistics.modpath.."/drone.lua")
