-- store reserved items by chest coordinates
-- so that drones will not attempt to fetch items that are already being fetched
local reservations = {}

-- lift a reservation for an ItemStack for Provider at given coords
function unreserve_item(item, coords)
    local c = minetest.serialize(coords)

    if not reservations[c] then
        print("Tried to unreserve Item " .. item:get_name() .. ", but item was not reserved in the first place")
        return
    end

    local itemname = item:get_name()

    if not reservations[c][itemname] then
        print("Tried to unreserve Item " .. item:get_name() .. ", but item was not reserved in the first place")
        return
    end

    local itemcount = item:get_count()

    if reservations[c][itemname] < itemcount then
        print("Tried to unreserve " .. itemcount .. " of Item " .. item:get_name() .. ", but only " .. reservations[c][itemname] .. "were reserved")
    end

    reservations[c][itemname] = reservations[c][itemname] - itemcount

    -- delete table key if possible
    if reservations[c][itemname] == 0 then
        reservations[c][itemname] = nil
    end
end

-- set a reservation for an ItemStack for Provider at given coords
function reserve_item(itemname, itemcount, coords)
    local c = minetest.serialize(coords)

    print("Reserving " .. itemname .. " " .. itemcount .. " at coords " .. c)

    if not reservations[c] then
        reservations[c] = {}
    end

    -- no items of this type reserved, just set count
    if not reservations[c][itemname] then
        reservations[c][itemname] = itemcount
        return
    end

    -- items of this type already reserved, add count
    reservations[c][itemname] = reservations[c][itemname] + itemcount
end

-- get number of reserved items of same type for Provider at given coords
function get_reserved_count(itemname, coords)
    local c = minetest.serialize(coords)

    if not reservations[c] then
        return 0
    end

    local count_for_item = reservations[c][itemname]

    if not count_for_item then
        return 0
    end

    return count_for_item
end
