dofile(logistics.modpath .. "/reservations.lua")
dofile(logistics.modpath .. "/chest.lua")

local function get_dest(t)
    return { x = t.x, y = t.y + 2, z = t.z }
end

local function on_provider_reached(self, target)
    local chest_inv = minetest.get_inventory({ type = "node", pos = self.target })

    if not chest_inv:contains_item("main", self.reqstack) then
        return
    end

    local removed_item = chest_inv:remove_item("main", self.reqstack)
    self.cargo = removed_item
    unreserve_item(self.reqstack, self.target)
end

local function on_requester_reached(self, target)
    local inv = self.requester:get_inventory()
    inv:add_item("main", self.cargo)
    self.cargo = {}
    self.object:remove()
end

local function on_target_reached(self, target)
    local node = minetest.get_node(self.target)

    -- remove items
    if node and node.name == "default:chest" then
        on_provider_reached(self, target)
        self.reqstack = nil
        self.target_mob = self.requester
        self.target = self.requester:get_pos()
        return
    end

    on_requester_reached(self, target)
    self.target = nil
end

local function move_to(self, target)
    local mypos = self.object:get_pos()


    -- calculate velocity
    local distance_target = vector.subtract(target, mypos)
    local distance_target_abs = vector.length(distance_target)

    local vel = { x = 0, y = 0, z = 0 }
    if distance_target_abs > 0.1 then
        -- animate
        local direction_target = vector.normalize(distance_target)
        local d = distance_target_abs
        local vscale = math.min(math.max(logistics.drone_vscale * d, logistics.drone_vmin), logistics.drone_vmax)

        vel = vector.multiply(direction_target, vscale)
    else
        -- jump and stop
        self.object:set_pos(target)
        on_target_reached(self, target)
    end

    look_at(self.object, target, 1.3)

    self.object:setvelocity(vel)

end

minetest.register_entity("logistics:drone", {
    visual = "mesh",
    mesh = "logistics_drone.b3d",
    physical = true,
    textures = { "logistics_drone.png", },
    visual_size = { x = 5, y = 5 },
    on_activate = function(self, staticdata)
        local data = minetest.deserialize(staticdata)

        if not data then
            return
        end

        self.cargo = data.cargo

        local reqstack = ItemStack(data.item)
        if not reqstack then
            return
        end

        local player = minetest.get_player_by_name(data.name)
        if not player then
            return -- Player is not online
        end

        self.requester = player
        self.reqstack = reqstack
    end,
    on_step = function(self, dtime)
        if not self.target and self.reqstack then
            self.object:setvelocity({ x = 0, y = 0, z = 0 })
            self.target = find_chest(self, self.reqstack)

            if not self.target then
                print("non-operable request - item not found")
                self.object:remove()
                return
            end

        end

        if not self.target then
            return
        end

        if self.target_mob then
            self.target = self.target_mob:get_pos()
        end

        move_to(self, get_dest(self.target))
    end,
    collisionbox = { 0, -0.1, 0, 0, 0.1, 0 },
})

minetest.register_chatcommand("logistics_request", {
    params = "[item]",
    description = "Request an item stack",
    privs = { ["server"] = true },
    func = function(name, itemstr)
        local player = minetest.get_player_by_name(playername)
        if not player then
            return -- Player is not online
        end

        local playerpos = player:get_pos()

        local item = ItemStack(itemstr)
        local item_count = item:get_count()

        local i = 0


    end
})


