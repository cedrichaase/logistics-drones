-- chest api functions

function read_chest_contents(pos)
    local inv = minetest.get_inventory({ type = "node", pos = pos })
    local list = inv:get_list("main")

    local count_by_itemname = {}

    local i, stack
    for i, stack in pairs(list) do
        repeat -- makes 'do break end' act like continue
            local name = stack:get_name()
            local count = stack:get_count()

            if not name or #name < 1 or not count then
                do break end -- continue
            end

            if not count_by_itemname[name] then
                count_by_itemname[name] = 0
            end

            count_by_itemname[name] = count_by_itemname[name] + count
        until true
    end

    return count_by_itemname
end

function get_chest_itemcount(pos, itemname)
    local chest_contents = read_chest_contents(pos)

    if not chest_contents[itemname] then
        return 0
    end

    return chest_contents[itemname]
end


-- find chest containing itemstack at position p with radius r
local function find_chest(p, r, itemstack)
    local itemname = itemstack:get_name()

    local pstart = { x = p.x - r, y = p.y - r, z = p.z - r }
    local pend = { x = p.x + r, y = p.y + r, z = p.z + r }

    local chests = minetest.find_nodes_in_area(pstart, pend, { "default:chest" })

    if not chests or #chests < 1 or not chests[1] then
        return nil
    end

    -- search for chest with requested item
    local _, chest
    for _, chest in ipairs(chests) do
        local chest_itemcount = get_chest_itemcount(chest, itemname)
        local available_count = chest_itemcount - get_reserved_count(itemname, chest)

        if available_count > 0 then
            return chest
        end
    end

    return nil
end
