LogisticsRequest = {}
LogisticsRequest.__index = LogisticsRequest

function LogisticsRequest.new(player, item, quantity)
    local self = setmetatable({}, LogisticsRequest)

    self.player = player
    self.item = item
    self.quantity = quantity
    self.quantity_unfulfilled = quantity

    self.drones = {}

    return self
end

function LogisticsRequest.fulfill(self)
    while self.quantity_unfulfilled > 0 do
        local n_to_remove = math.min(self.quantity_unfulfilled, logistics.drone_cargo_size) -- at most cargo size

        local itemstring = item:get_name() .. " " .. n_to_remove

        local staticdata = { name = name, item = itemstring, cargo = {} }
        local drone = minetest.add_entity(vector.add(get_dest(playerpos), random_vector(6)), "logistics:drone", minetest.serialize(staticdata))

        item_count = item_count - n_to_remove
        i = i + 1
    end
end
