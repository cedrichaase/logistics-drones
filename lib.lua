function dump(o)
    if type(o) == 'table' then
        local s = '{ '
        for k, v in pairs(o) do
            if type(k) ~= 'number' then
                k = '"' .. k .. '"'
            end
            s = s .. '[' .. k .. '] = ' .. dump(v) .. ','
        end
        return s .. '} '
    else
        return tostring(o)
    end
end

function printd(o)
    print(dump(o))
end


-- let object (ObjectRef) look at target (ObjectRef)
-- offset in radians
function look_at(object, target_pos, offset)
    if not offset then
        offset = 0
    end

    local object_pos = object:get_pos()

    -- calculate the orientation
    local dir = vector.direction(target_pos, object_pos)
    local yaw = math.atan(dir.z / dir.x) + math.pi / 2
    if target_pos.x > object_pos.x then
        yaw = yaw + math.pi
    end

    object:setyaw(yaw + offset)
end

function find_in_table(table, predicate)
    for k, v in pairs(a) do
        if predicate(v) then
            return v
        end
    end
end

function random_vector(n)
    math.randomseed(os.clock())
    math.random(); math.random(); math.random();
    return { x = math.random(n), y = math.random(n), z = math.random(n) }
end


